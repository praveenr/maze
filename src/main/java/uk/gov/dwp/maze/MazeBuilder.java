package uk.gov.dwp.maze;


public interface MazeBuilder {

    Maze build();
}
